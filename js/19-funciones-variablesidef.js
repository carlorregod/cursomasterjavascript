//Funciones tipo rest son aquellas que peudes pasarle indefinida cantidad de variables

//referencia: index.html

//ello se hace con la "variable arreglo" ...{nombre}
/* Ejemplo de esto
function mifuncion(a,b,c, ...otros){
    console.log(otros[0]); //colección de datos
} */

function frutas(...fruta) {
    console.log(fruta);
}

frutas('tomate', 'manzana', 'naranja', 'pera', 'cereza'); //respuesta: ["tomate", "manzana", "naranja", "pera", "cereza"]

//Los spreads sirven para añadir elementos de un arreglo a otro, por ejemplo
fruta = ['Toronja', 'Limón'];

//A la función fruta se añadirán ahora la variable frutaañadirá los elementos del arreglo y los pusheará
//Primer caso, 
frutas(...fruta, 'tomate', 'manzana', 'naranja', 'pera', 'cereza');  //respuesta: ["Toronja", "Limón", "tomate", "manzana", "naranja", "pera", "cereza"]
//Al no añadir los ..., se crea un espacio de un arreglo que incluye otro arreglo
frutas(fruta, 'tomate', 'manzana', 'naranja', 'pera', 'cereza'); //respuesta: [["Toronja", "Limón"], "tomate", "manzana", "naranja", "pera", "cereza"]