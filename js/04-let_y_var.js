//referencia: index.html

'use strict'
console.log('Viendo las var');
var numero = 20;
    console.log(numero);
if (true) {
    numero = 40;
    console.log(numero);  
}
console.log(numero);
/* Viendo ahora con let */
console.log('Viendo ahora con let');

let num = 20;
    console.log(num);
if (true) {
    let num = 40;
    console.log(num);  
}
console.log(num);

/* Esto es lo que imprime
Viendo las var
04-let_y_var.js:4 20
04-let_y_var.js:7 40
04-let_y_var.js:9 40
Viendo ahora con let
04-let_y_var.js:14 20
04-let_y_var.js:17 40
04-let_y_var.js:19 20
*/