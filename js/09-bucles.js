"use strict"

//referencia: index.html

//Bucle son estructurasde control que se repiten varias veces

for(let i=1;i<=10;i++)
{
    console.log(i);
}

console.log("**************\r");

var numero =5;
while(numero != 10)
{
    numero+=1;
    console.log(numero);
}

console.log("**************\r");

var x=5;
do
{
    x-=1;
    console.log(x); //Pasaráa por acá aunque x=5>4 porque do while antes de iniciar el bucle, pasa por el la primera vez
}while(x<4);