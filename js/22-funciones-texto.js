'use strict'
//referencia: index.html
//Variables para transformarlas en texto
var numero=444;
var texto1='eS uN ejeMpLo de ejeRciciO';
var texto2="Muy interesante";

//De número a string
/* 1 */ console.log(numero.toString());
/* 2 */ console.log(String(numero));

//Todo en MAYÚSCULAS
console.log(texto1.toUpperCase());

//Todo en minúsculas
console.log(texto1.toLowerCase());

//Capitalizar primera letra, el resto en minúsculas
console.log(texto1.charAt(0).toUpperCase() + texto1.slice(1).toLowerCase());

//Longitud de texto
console.log(texto1.length); //el método length arroja error si es null o no se define el argumento. Sirve para arrays

//Concatenando texto
/* 1 */console.log(numero + texto1 + texto2);
/* 2 */console.log(String(numero).concat(texto1,texto2));

//Buscando...

/* 1 */console.log(texto1.indexOf('eje'));  //Resulta 6... eS uN e...je... 0-1-2-3-4-5-6...7-8 parte de cero!
/* 2 */console.log(texto1.search('eje'));  //Equivalente al indexOf...
console.log(texto1.lastIndexOf('eje')); //Da 17 porque buscará la última coincidencia de donde se encuentra el caracter.

console.log(texto1.indexOf('<'));  //Cuando no encuentra el caracter, arrojará -1

console.log(texto1.match('eje'));  //Devuelve el resultado como un array ["eje", index: 6, input: "eS uN ejeMpLo de ejeRciciO", groups: undefined]
console.log(texto1.match(/eje/gi)); //Devolverá un arreglo con las coincidencias: ["eje", "eje"]

console.log(texto1.startsWith('eje')); //Devuelve false porque ha encontrado 'eje' al iniciar la frase. Caso contrario retorna true
console.log(texto1.endsWith('ejeRciciO')); //acá retorna true porque la frase termina en esa palabra
console.log(texto1.includes('eje'));  //Busca la cadena dentro de texto1. como 'eje' si existe, retorna true
// console.log(texto1.test('eje'));  //Dara verdadero, sólo analiza si se encuentra o no el caracter

//Sustraendo caracteres

console.log(texto1.substr(3,7)); //Esto significa, a contar de la psición 3, quitar 7 catacteres: uN ejeM
console.log(texto1.charAt(4)); //Extrae el cuarto caracter de texto 1: N

//Operaciones con texto con reemplazos
console.log(texto1.replace('eje', 'ojo')); //sustituirá una vez'eje' por 'ojo'
console.log(texto1.replace(/eje/g, 'ojo')); //todo lo que dice 'eje' se sustituye por ojo
console.log(texto1.replace('ejeMpLo', 'EjerCicIo')); //sustitución de una palabra completa

console.log(texto1.slice(5,15)); //Separa texto entre 2 posiciones de index, acá, entre 5 y 15

//Metiendo los caracteres de un string en un array
console.log(texto1.split(" "));  //Respuesta: ["eS", "uN", "ejeMpLo", "de", "ejeRciciO"], separador es de espacios

//Quita espacios en cadenas de texto antes y después
var texto3= "  espacios de frases locas   ";
console.log(texto3);
console.log(texto3.trim());  //Ve la diferencia???
