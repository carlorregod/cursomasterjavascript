//referencia: index.html
"use strict"

var numero =8;

switch(numero)
{
    case 0:
        console.log('Es cero');
        break;
    case 1:
        console.log('Es uno');
        break;
    case 8:
        console.log('Es un ocho');
        break;
    default:
        console.log('No es cero, uno u ocho');
        break;
}