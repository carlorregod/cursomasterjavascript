'use strict'
//referencia: index.html
function suma(a, b, sumando, sumax2)
{
    var adicion= a+b;
    sumando(adicion);
    sumax2(adicion);
}

//sumando y sumax2 son parametros llamados desde un callback

suma(3, 4, 
    (sum) => {
        console.log('La suma es '+sum);
    },
    (sum) => {
        console.log('La suma doble es '+sum*2);
    });

/* 
La notación de las funciones callback expresadas como flecha
(sum) => {
        console.log('La suma es '+sum);
    }
EQUIVALE A
function(sum){
    console.log('La suma es '+sum);

NOTAR QUE UNA NOTACIÓN FLECHA O TRADICIONALES ES DE ELECCIÓN DE QUIEN PROGRAE
} */

suma(3, 4, 
    function(sumandolo) {
        console.log('La suma triple es '+sumandolo*3);
    },
    function(sum) {
        console.log('La mitad de la suma, aproximada es '+Math.round(sum/2,1));
    });