'use strict'

//asociado con asincrono.html

//try-catch permite probar y ejecutar código fuente. Si hay errores puede atraparse

try
{
    console.log('intento de-try-catche');
    var x =3, y=0;
    if(x/y == Infinity)
        throw('error division por cero'); //maneja tus excepciones!!!
    else
    console.log(x/y);
    console.log('fin del código');
}
catch(e)
{
    console.log('Error encontrado: '+e);
}

//Ejemplo sin manejo de excepciones por defecto
try
{
    console.log(decodeURIComponent('https://ww#%w.g_ÑP??¡)(&óñnC`e.com')); //error!
    console.log(z); //otro error!
}
catch(e)
{
    console.log(e); //z no está declarado o el del decodeURI??? muestra el primero osea el decodeURI