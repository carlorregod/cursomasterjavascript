'use strict'
//referencia: index.html
var dom = document.getElementById('div1');
console.log(dom);
console.log(dom.innerHTML);

//Seteando otras propiedades, por ejemplo value
dom.classList = 'qué tal';
dom.setAttribute('class','ota');
dom.setAttribute('value',33);
console.log(dom);
dom.style.padding = '20px';
dom.style.backgroundColor = 'red';
dom.innerHTML = 'Texto ha camviado';

//Sin jquery, se puede atrapar lo del js con el queryselector

var caja = document.querySelector('#div1');
console.log(caja);

//Conseguir los elementos de cada div
var todos_los_div = document.getElementsByTagName('div');
console.log(todos_los_div);
//Hagamos un recorrido...
console.log('Recorriendo los todos_los_div...:');
//Si no se antepone el método Array.from... arrojará error
Array.from(todos_los_div).forEach(element => {
    console.log(element);
});
//Modificando texto en alguno de los elementos del dom capturando alguno de los elementos...
todos_los_div[2].innerHTML='Te cambio el texto waja';

//Conseguir elementos por su clase
var div_por_clase = document.getElementsByClassName('clase_prueba');
console.log(div_por_clase);
//Modificamos elementos recorriendo los elementos con un confiable foreach
var i=1;
Array.from(div_por_clase).forEach(e => {
    e.innerHTML = i;
    i+=1;
});
//también se pueden recorer de esta manera
Array.from(div_por_clase).forEach((valor, index) => {
    var parrafo = document.createElement('p'); //Creamos un párrafo
    var texto = document.createTextNode(valor.innerHTML);
    parrafo.appendChild(texto);
    parrafo.setAttribute('class', 'nuevaclase');
    parrafo.style.background='red';
    document.querySelector('#seccion1').appendChild(parrafo);   //Esto es equivalente al document.getElemetById de toda la vida..
});