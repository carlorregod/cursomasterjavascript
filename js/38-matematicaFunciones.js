'use strict'
//referencia: vistas/fecha_num.html
while(true)
{
    var numero = parseFloat(prompt('Dame un número, un número decimal será aceptado'));
    if(!isNaN(numero))
        break;
    else
        alert('Ingrese un valor numérico');
}
//Operaciones matemáticas
var resultado =`
Numero ingresado: ${numero},
Potencia al cuadrado: ${Math.pow(numero,2)},
Raíz: ${Math.sqrt(numero)},
Num truncado:  ${Math.ceil(numero)},
Num aproximado:  ${Math.round(numero)},
Num seno:  ${Math.sin(numero)},
Num raiz cubica:  ${Math.cbrt(numero)},
Num valor absoluto:  ${Math.abs(numero)},
FIN!
`;
console.log(resultado);
alert(resultado);