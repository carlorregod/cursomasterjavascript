//referencia: index.html
"use strict"

// Condicional IF
// Si A se compara con B, se toman decisiones

var edad = 12;
var nombre = "Carlos Orrego";

if(edad != nombre) {
    informacion("No es lo mismo "+edad+" que "+nombre);
}



/* Operadores
== igual
!= distinto
> mayor
< menor
>= mayor o igual
<=menor o igual
=== exactamente igual

*/
//Otro if de ejemplo
if(edad === 18) {
    informacion("Tienes exactamente 18 años");
} else if(edad >18){
    error("Eres mayor de edad");
} else {
    warning("Eres menor a 18 años");
}

iziToast.info({
    timeout: 20000,
    overlay: true,
    displayMode: 'once',
    id: 'inputs',
    zindex: 999,
    title: 'Inputs',
    message: 'Examples',
    position: 'center',
    drag: false,
    inputs: [
        ['<input type="checkbox">', 'change', function (instance, toast, input, e) {
            console.info(input.checked);
        }],
        ['<input type="text">', 'keyup', function (instance, toast, input, e) {
            console.info(input.value);
        }, true],
        ['<input type="number">', 'keydown', function (instance, toast, input, e) {
            console.info(input.value);
        }],
    ]
});