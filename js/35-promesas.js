'use strict'

window.onload = function(){
    alert('Mensaje 1');
    obtenerNombre()
    .then(data=>JSON.parse(data))
    .then(data=>imprimirNombre(data))
    .then(alert('terminado'));
}


//referencia: asincrono.html
var perfil={
    nombre:'Carlos',
    edad:27,
    comuna:'Macul'
};

function obtenerNombre(){
    return new Promise((resolve, reject)=>{
        var perfil_string = JSON.stringify(perfil);

        if(typeof(perfil_string) != 'string')
            return reject('error');
        else
            return resolve(perfil_string);
    });
}

function imprimirNombre(dato){
    return new Promise((exito, fracaso)=>{
        if(dato.nombre)
            return exito(document.querySelector('#cosas').innerHTML=dato.nombre);
        else 
            return fracaso('error');
    });
}