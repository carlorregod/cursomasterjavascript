'use strict'
//referencia: asincrono.html
function cargapkmn(a,b) {
    if(document.querySelector('#pokemon').hasChildNodes())
        document.querySelector('#pokemon').firstChild.remove();//limpieza inicial
    var lista = document.createElement('ul');
    fetch('https://pokeapi.co/api/v2/pokemon/?offset='+a+'&limit='+b)
    .then(data=> data.json())
    .then(data=> {
        var resultado = data.results; //Un array de json varios e pokes
        console.log(resultado);
        for(var i in resultado)
        {
            var poke = document.createElement('li');
            poke.append(resultado[i].name);
            lista.append(poke); //Se genera una lista con los poke a imprimir
        }
        document.querySelector('#pokemon').append(lista);
    });
}


document.querySelector('#btnMostrar').addEventListener('click',()=> {
    var numero_inicial=(parseInt(document.querySelector('#inicial_pkmn').value) -1).toString();
    var cantidad=document.querySelector('#cantidad_pkmn').value.toString();
    cargapkmn(numero_inicial, cantidad); 
});


