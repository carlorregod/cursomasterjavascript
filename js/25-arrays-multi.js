'use strict'
//referencia: index.html
var categoria = ['comedia', 'drama', 'terror'];

var peliculas = [
    'la verdad duele',
    'las esquinas redondas',
    'trancada de trasero III'
];

//EN el fondo esto es una matriz
var cinema = new Array(categoria, peliculas);

console.log(cinema);

console.log(cinema[0][1]);


//Operaciones con arrays
peliculas.push('Super rat');
console.log(peliculas);


//Agregación dinámica de elementos a un array;

var coleccion = [];
do{
   var item = prompt('Ingrese una pelicula. Si no desea más peliculas, ingresar FIN','asd');
   coleccion.push(item);
}while(item != 'FIN');

coleccion.pop(); //elimina el último elemento del array
console.log(coleccion);

//indefine un elemento
coleccion[0] = undefined;
//elimina un elemento en cierto index
coleccion.splice[0,1]; //elimina el elemento index 0
coleccion.shift(); //remueve el primer elemento
delete coleccion[2]; //elimina el elemento 2 del arreglo. Esto sirve para eliminar elementos con index distintos de números. Se podría ordenar con el método sort
console.log(coleccion);


//Eliminando un elemento del array
var arregloso = ['manzana', 'pera', 'uva', 'melon'];
var pera = arregloso.indexOf('pera'); //Buscamos el index de pera, debería ser 1
arregloso.splice(pera,1);
console.log(arregloso);
