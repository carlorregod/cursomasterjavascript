'use strict'
//referencia: index.html
window.onload = function() {
    // eventos del ratón
    var boton = document.querySelector('#btnEvento'); //Elemento boton Presioname
    boton.style.background= 'blue';

    function cambiaColor() {
        boton.style.background = (boton.style.background =='blue')?'red':'blue';
        return;
    }
    //URL de eventos: https://www.w3schools.com/js/js_events.asp
    boton.addEventListener('click', cambiaColor);

    var listaclase= document.querySelectorAll('.clase_prueba');
    //recorriendo clases y asignándole cositas
    Array.from(listaclase).forEach(element => {
        element.addEventListener('mouseover', ()=>{element.style.background = 'green';});
        element.addEventListener('mouseleave', ()=>{element.style.background = 'blue';});
    });
    //Al pasar encima el back se vuelve verde y al salir, se vuelve azul

    //El temadel input llamado texto
    var texto = document.getElementById('texto'); 
    texto.addEventListener('keypress',()=> alert('presionaste algo!'));

    //Los temporizadores son ciclicos si se emplea setInterval. Pero si se usa setTimeout sólo se repite una vez
    var tiempo = setInterval(()=>{
        document.getElementById('vacio').innerHTML = document.getElementById('vacio').innerHTML == 'Texto temporizado' ? 'Texto temporizado 2': 'Texto temporizado';
    },500);

    document.getElementById('stop').onclick = ()=>{
        clearInterval(tiempo);
    };
};

/* Lista de eventos
blur
focus
keydown
keypress
keyup
 */
