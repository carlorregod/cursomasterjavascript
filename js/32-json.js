'use strict'
//localstorage.html
var pelicula = {
    titulo: 'batman',
    anio: 1992,
    pais: 'estados unidos'
};

console.log(pelicula);  //Ecritura del json
console.log(pelicula.titulo); //Imprime el titulo de la pelicula

pelicula.titulo = 'superman';
console.log(pelicula.titulo); //Ahora cambia titulo

//Array que almacena json
var peliculas = [
    {titulo: 'batman', anio: 1700,  pais: 'inglaterra'},
    pelicula
];

console.log(peliculas); //imprime ambos json

for (const key in peliculas) {
    var p = document.createElement('p');
    p.append(peliculas[key].titulo+'-'+peliculas[key].anio);
    document.querySelector('#peliculas').append(p)
}

