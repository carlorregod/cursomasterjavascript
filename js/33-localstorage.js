'use strict'
//localstorage.html

//A mostrar los elementos
function cargarLista() {
    var div_mensaje = document.querySelector('#coleccionNombres');
    var listado = document.createElement('ul');
    for(var i in localStorage){
        if(typeof(localStorage[i])=='string') //Sólo recorremos el txt
        {
            var li = document.createElement('li');
            li.append(localStorage[i]);
            listado.append(li);
        } 
    div_mensaje.append(listado);
    }
};

function actualizaLista() {
    document.querySelector('#coleccionNombres').firstChild.remove();
    cargarLista();
}


//Agregando elementos...
$('#btnEnviarNombre').click(()=>{
    let nombre = $('#nombre').val();
    if(typeof(nombre)=='string' && nombre !='' && nombre != null)
    {
        localStorage.setItem(nombre,nombre); //Añadiendo elemento al localstorage
        $('#nombreHelp').html('Dato ingresado exitosamente');
        $('#nombreHelp').show(); 
        setTimeout(() => {
            $('#nombreHelp').hide();
        }, 2000);
        actualizaLista();
    }
    else
    {
        $('#nombreHelp').html('Ingrese un valor válido');
        $('#nombreHelp').show();
        setTimeout(() => {
            $('#nombreHelp').hide();
        }, 2000);
    }
        
});

//Borrando elemento
document.querySelector('#btnBorrarNombre').addEventListener('click',()=>{
    let borrador = document.querySelector('#nombre_borrar').value; //almacenamos el borrador
    if(localStorage.getItem(borrador) != null && localStorage.getItem(borrador) !='')//Si existe...
    {    
        localStorage.removeItem(borrador); //Adios elemento
        $('#nombreBorradoHelp').html('Elemento borrado');
        $('#nombreBorradoHelp').show();
        setTimeout(() => {
            $('#nombreBorradoHelp').hide();
        }, 2000);
        actualizaLista();
    }
    else
    {
        $('#nombreBorradoHelp').html('Valor no existe en los datos almaceados');
        $('#nombreBorradoHelp').show();
        setTimeout(() => {
            $('#nombreBorradoHelp').hide();
        }, 2000);
    }
        

});


window.onload = ()=>{
    cargarLista();
};


