//referencia: index.html

'use strict' //Usar para modo estricto

try {
pais = 'España';  //pais no se define en modo estricto si no se usa el var
var continente = 'Europa';
var pais_continente = pais+" "+continente;
console.log(pais_continente);
}
catch(Exception) {
    console.log('Error por modo estricto por:',Exception);
}