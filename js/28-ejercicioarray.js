'use strict'
//referencia: index.html
/* Realice lo siguiente
a)Un arreglo el cual será completado por el usuario con 6 números
b)Imprimir el valor de cada elemento del array, pero ordenado de mayor a menor 
c)Invertir el orden y mostrar ello
d)Mostrar la cantidad de elementos de un array
e)Buscar un número dentro del array y entregar el index*/

//Funciones de este ejercicio
var imprimearreglo = function(array, texto='') {
    let arr = '';
    for(let index in array) {
        arr +='<li>'+array[index]+'</li>';
    }
    let plantilla =
        `<h1>Muestra del arreglo ${texto}</h1>
        <ol>
            ${arr}
        </ol>`;
    return document.write(plantilla);
};

var arreglo = new Array(6); //Inicializamos el array con 6 slots

//La letra a...
for(var i=0; i<6;i++)
{
    arreglo[i] = validanumero(i+1); //Función de librería que retorna un entero
}
//Para el último ejercicio, usaremos esto
var arreglo_default = arreglo;
imprimearreglo(arreglo,'por defecto');
//La letra b...
imprimearreglo(arreglo.sort((a,b)=>b-a),'ordenados'); //Ordena números desde mayor hasta el menor
//La letra c...
imprimearreglo(arreglo.reverse(),'invertido'); //Muetsra pero el arreglo ya ordenado, ahora a la inversa
//La letra d...
document.write('<hr><p>Cantidad de elementos del arreglo: '+arreglo.length+'</p>');  //Debería ser =6
//La letra e
var numero_buscar = validanumero('');
if(arreglo.some(numero=>numero==numero_buscar))
    document.write('<hr><p>El valor: '+numero_buscar+' se encuentra en el array en la posición original: '+(arreglo_default.findIndex(posicion=>posicion==numero_buscar)-1)+'</p>');
else
    document.write('<hr><p>El valor: '+numero_buscar+' no se encuentra en el array</p>');