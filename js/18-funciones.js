//referencia: index.html

function holaMundo() {
    return console.log('hola mundo');
}

//Con parámetro
function suma(a, b) {
    return (a + b);
}
function resta(a, b) {
    return (a - b);
}
function mult(a, b) {
    return (a * b);
}
function div(a, b) {
    return (a / b);
}

function calcula()
{
    numero1 = validanumero(1);
    numero2 = validanumero(2);
    alert('Resultados obtenidos: \nSuma: '+suma(numero1,numero2)
        + '\nResta: '+resta(numero1,numero2)
        +'\nMultiplicación: '+mult(numero1,numero2)
        +'\nDivisión: '+div(numero1,numero2));

}

//Funciones anidadas
// Cuerpo

holaMundo();

calcula();