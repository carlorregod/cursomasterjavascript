/* Librería que se debe cargar antes que los métodos personales */

function informacion(mensaje = "", title = "Información", timer= 2000) {
    iziToast.info({
        'title': title,
        'message': mensaje,
        'timeout': timer,
        'position': 'center',
    });
}

function error(mensaje = "Error", title = "Información", timer= 2000) {
    iziToast.error({
        'title': title,
        'message': mensaje,
        'timeout': timer,
        'position': 'center',
    });
}

function warning(mensaje = "", title = "Advertencia", timer= 2000) {
    iziToast.warning({
        'title': title,
        'message': mensaje,
        'timeout': timer,
        'position': 'center',
    });
}

function formulario(mensaje = "", title = "Advertencia") {
    iziToast.question({
        timeout: 20000,
        close: false,
        overlay: true,
        displayMode: 'once',
        id: 'question',
        zindex: 999,
        title: title,
        message: mensaje,
        position: 'center',
        buttons: [
            ['<button><b>YES</b></button>', function (instance, toast) {
     
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
     
            }, true],
            ['<button>NO</button>', function (instance, toast) {
     
                instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
     
            }],
        ],
        onClosing: function(instance, toast, closedBy){
            console.info('Closing | closedBy: ' + closedBy);
        },
        onClosed: function(instance, toast, closedBy){
            console.info('Closed | closedBy: ' + closedBy);
        }
    });
}

/* Inportar script de js dentro de otro js */
// en jquery se puede usar $.getScript("script.js");

function importarScript(nombre) {
    var s = document.createElement("script");
    s.src = nombre;
    document.querySelector("head").appendChild(s);
}


//Valida que la entrada sea entero positivo
function validanumero(index) {
    do 
    {
        var num =  prompt('Ingrese un número positivo '+index,'0');
        var num1 = num.replace(/[,]/g,"."); //Sacamos las comas en el caso y sustituimos por decimales
        //Validaciones...
        if(num == "") //Espacio en blanco
                alert('Ingrese algo... espacio en blanco');
        else if(isNaN(parseFloat(num)))  //Reconocimiento de cadenas
            alert('Ingrese un número entero no una cadena');
        else if(parseFloat(num) != parseFloat(num1) || parseFloat(num)%1 != 0) //Descartando decimales
            alert('No números decimales');
        else
        {
            numero = parseInt(num);
            if( numero <= 0 )  //Descartando enteros negativos o el cero
                alert('Ingrese un número positivo');
            else 
            {   
                return numero;
            }
        }
    
    }while(true);
}

Capitalize = function(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase()
}
