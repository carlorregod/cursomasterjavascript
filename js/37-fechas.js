'use strict'
//referencia: vistas/fecha_num.html
var fecha= new Date(); //Asi tal cual es la fecha del sistema

var anio= fecha.getFullYear();//Saca el año
var mes= fecha.getMonth()+1; //Saca el mes 0:enero en adelante, sumar siempre 1
var dia = fecha.getDate(); //Saca el día
var horas = fecha.getHours(); //Saca la hora
var minutos = fecha.getMinutes(); //Saca minutos

var calendario = `
La fecha es ${dia} del ${mes} del ${anio} y son las ${horas} con ${minutos}
`;

console.log(calendario);
console.log(fecha);

/* Otro ejemplo */
console.log(new Date(1995,11,17)); //Esto equivale a: 17 de diciembre de 1995. LOs meses empiezan desde cero. Mes 12 no existe
console.log(new Date(1995,11,17,23,55,11)); //Esto equivale a decir: 17 de diciembre de 1995 a las 23:55:11
console.log(new Date("December 17, 1995 03:24:00"));
console.log(new Date('2019-11-22')); //Esto lo intepreta como año-dia-mes, en este caso hay que restar 1 al dia
console.log(new Date('2019-11-22').toUTCString());//Esto corregirá la suma de un día del caso anterior al dejar el formato horario de la forma GMT
console.log(new Date('dd-mm-YY', '22-06-1982')); //Arrojará un error, se debe invertir la fecha para ello

//Los errores de fechas del formato dd-mm-yy o dd/mm/yy podrían corregirse de esta manera
function corregirFecha(date)
{
    let fecha_array;
    //date puede ser dd-mm-yy o dd/mm/yy como string
    if(date.search(/[-]/g,date) != -1) //Si es del formato dd-mm-yy
        fecha_array= date.split('-');
    else if(date.search(/[/]/g,date) != -1) //Si es del formato dd/mm/yy
        fecha_array = date.split('/');
    else //En caso de ingresar algo errado...
        return console.log('Error');
    return new Date(fecha_array[2],fecha_array[1]-1,fecha_array[0],17,0,0); //Se corrige el formateo horario agregando las 17 horas
}
