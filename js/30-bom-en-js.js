'use strict'

//referencia: index.html

// BOM:Browser object model
var getBom = function() {
    alert('Medidas de la ventana: '+ window.innerWidth +'x'+ window.innerHeight); //Medidas de la ventana
    alert('Url actual : '+window.location);  //Url actual.también se puede escribir comowindow.location.href
    console.log(window.location);  //Acá si se ve todo el array. Acá si ha que agregar el href. window.location.href
};

var redirect = function() {
    let url = prompt('Escriba una url', 'http://www.google.cl');
    window.location.pathname = url;
    // location.href = url;
};

function redirigir(url) {
    window.location.href=url;
}

function redirabrirVentanaigir(url) {
    window.open(url,"","width=300,heigth=200");
}
//Con un botón se probará esto
document.querySelector('#btnEvento').addEventListener('click', getBom);
//Otro botón que redirigira el navegador a otra página
document.querySelector('#redirige').addEventListener('click', redirect);
