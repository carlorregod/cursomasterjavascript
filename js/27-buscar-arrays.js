'use strict'
//referencia: index.html
var mensaje_arreglo = ['un',
'mensaje',
'hecho',
'con',
'amor',
'y',
'dedicación'];

//Para buscar usamos find... y si encuentra algo retornará verdadero
console.log(mensaje_arreglo.find((mensaje)=>{
        return mensaje == 'con';
    })
); //retorna con

//Un caso falso
console.log(mensaje_arreglo.find((mensaje)=>{
        return mensaje == 'nyan';
    })
); //retorna 'undefined'
//Forma resumida
console.log(mensaje_arreglo.find(mensaje => mensaje == 'amor'));

//Retorno del index
console.log(mensaje_arreglo.findIndex(mensaje => mensaje == 'amor')); //retornará 4
console.log(mensaje_arreglo.findIndex(mensaje => mensaje == 'hamor'));//retornará -1. NO se encuentra 'hamor'

var array_num = [3,5,1,34,12,8,80,44];
//Buscando en números
console.log(array_num.some(montos => montos >20)); //retorna true pues el 34,44 y 80 son mayores a 20
console.log(mensaje_arreglo.some(arg => arg == 'hecho')); //también sirve con strings, acá si retorna true