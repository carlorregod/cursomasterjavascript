//Realice un programa que comprueba la paridad o no de un número
//El usuario puede ingresar cualquier cosa, string, caracteres, espacios en blanco,...
//referencia: index.html
'use strict'

var num, num1, numero;
do{
    
    num =  prompt('Ingrese un número positivo','Ingreso');
    num1 = num.replace(/[,]/g,"."); //Sacamos las comas en el caso y sustituimos por decimales
    //Validaciones...
    if(num == "") //Espacio en blanco
            alert('Ingrese algo... espacio en blanco');
    else if(isNaN(parseFloat(num)))  //Reconocimiento de cadenas
        alert('Ingrese un número entero no una cadena');
    else if(parseFloat(num) != parseFloat(num1) || parseFloat(num)%1 != 0) //Descartando decimales
        alert('No números decimales');
    else
    {
        numero = parseInt(num);
        if( numero <= 0 )  //Descartando enteros negativos o el cero
            alert('Ingrese un número positivo');
        else 
        {   
            //Desarrollo del problema
            var respuesta = (numero%2 == 0) ? 'par' : 'inpar';
            alert('El número '+ numero+' es: '+respuesta);
            break;
        }
    }
    console.log(num);
        console.log(numero)
    
}while(true);