'use strict'
//referencia: index.html
//Arreglo: colección de elementos que tienen un indexador y un valor en él
//Se puede usar el método new Array() o los corchetes []

var arreglo = [1,2,3,4,5,6,7,'a'];

cons]ole.log(arreglo[0]);

var arreglo2 = new Array('a', 'b', 'c');

console.log(arreglo2[0]);

//contar cant de elementos
console.log(arreglo.length);

//Agregar elementos en array
arreglo.push('hola');
//Recorremos e imprimimos los valores. Bucle foreach nativo
console.log('*Con Foreach*');
var index = 1;
arreglo.forEach(element => {
    console.log('Índice '+index+' :'+element);
    index +=1;
});
console.log('*Con For*');
//Otra forma nativa es con bucle for
for(var i=0; i<arreglo.length; i++)
{
    console.log('Índice '+(i+1)+' :'+arreglo[i]);
}
//Ahora recorriendo con for in...
//Muy útil para saber el valor de los subíndices de un arreglo
console.log('*Con for in*');
for (const key in arreglo) {
    console.log(arreglo[key]);
}
