'use strict'
//referencia: index.html
//Un arreglo a texto
var arreglo = ['Habia','una','vez','truz'];

console.log(arreglo);

var arreglo_string = arreglo.join();

console.log(arreglo_string);
//Para que quede como una operación sin comas
console.log(arreglo_string.replace(/[,]/g,' '));

//Inversa, de una cadena a un array
var texto = 'Una comida saludable es buena';
//Necesitamos incluir comas como separador
texto = texto.replace(/[ ]/g,',');
//Ahora sí, usamos el comando
var texto_arreglo = texto.split(',');
console.log(texto);
console.log(texto_arreglo);

/*  */
//Ordenando arreglos
console.log(texto_arreglo.sort()); //Orden alfabético, primero mayúsculas y después minúsculas
//Invertir elementos de un array
console.log(texto_arreglo.reverse());