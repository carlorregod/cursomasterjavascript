'use strict'
//referencia: vistas/textos.html

/* Textos */
$(document).ready(function(){
    //Atrapando el mensaje en el párrafo p de name=mensaje_inicial
    console.log($('[name=mensaje_inicial]').html())
    //El botón por defecto en el formulario viene en modo disabled, para quitr ese atributo se hace esta línea
    $('#add_enlace').removeAttr('disabled');
    //El método addAttr() agrega nuevos atributos
    console.log($('a').length); //Se  contarán los elementos de etiqueta 'a', las anclas
    //debugger
    //Autocompletando los txt forma 1:
    // $('a').each(function (indexInArray, valueOfElement) {  //recorriendo cada elemento de 'a'
    //     $(this).html($(this).attr('href'));
    // });
    //Autocompletando los txt forma 2:
    $('#lista').children().each(function(index, value){ //Acá se atrapan los "hijos" de la ul de id=lista
        $(this).children(0).html($(this).children(0).attr('href')); //Atrapa al primer hijo
        //.attr() implica el primer atributo 
    });

});

//Agregando nuevos elementos a la lista
$('#add_enlace').click(function(){
    if($('#nuevo_enlace').val() != '')
    {
        $('#lista').append('<li><a href="http://'+$('#nuevo_enlace').val()+'">'+$('#nuevo_enlace').val()+'</<a></li>');
    }
});
//append(): encrusta el contenido en la posiciona como ultimo elemento de los hijos de la referencia
//prepend():encrusta el contenido en la posicion hijo inicial 
//before(): encrusta el contenido antes del nodo, como hermano de la referencia
//after(): encrusta el contenido después del nodo como hermano
//children(): un hijo o colección de hijos de un nodo
//eq(): una posición determinada de un hermano, eq(0) es el primero
//next(): hermano siguiente
//prev(): hermano anterior
//parent(): padre directo
//parents(): ancestro inicial
//find(): para encontrar dentro del nodo (hijos o nietos, etc..) una referencia


/* Animaciones */
var cuadradito = $('#elemento_animar');
$('#trans_elemento').click(function(){
    cuadradito.toggle(); 
    //.toggle() aparecerá y desaparecerá el elemento
    //.fadeTo: aparecerá y desaparecerá los hijos del elemento
    //.hide() : siempre ocultará el elemento, es como un display="none"
    //.show() : siempre mostrará el elemento, es como un display="block"
    //.fadeIn(): siempre mostrarpa el contenido de los hijos del elemento
    //.fadeOut: siempre ocultará los elementos del los hijos del nodo
    /* Dentro de los argumentos se pueden pasar 2 variables: velocidad y tiempo
    'fast', 'slow', 'normal' son velocidades, 3000 son 3 segundos
    ejemplo .fadeIn('slow',4000)*/
    
});
//Se llama al elemento dummy y se le seteará un color
$('#dummy').css('background','red');
//cambiar el color del elemento
$('#color_elemento').click(function(){
    if(cuadradito.css('background') ==  $('#dummy').css('background')) // Comparamos si el background es rojo
        cuadradito.css('background','blue');
    else
        cuadradito.css('background','red');
});

//Desplazar el element
$('#desplaza_elemento').click(function(){
    cuadradito.animate({
        marginLeft:'400px', 
        height:'40px', 
        width:'40px',
        marginTop:'300px'}
        ,'slow')
        .animate({marginLeft:'0px',
                marginTop:'0px'},
                'fast',
                function(){
                    console.log('Ciclo acabado');
                    });
});

//todos estos métodos de animación admiten un tercer parámetro como función callback para efectuar otras acciones
//el console.log('ciclo acabado') se ejecuta cuando termina la anmación
//Una animación puede encadenarse con otras