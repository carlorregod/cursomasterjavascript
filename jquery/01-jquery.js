'use strict'
//referencia: vistas/index-jquery.html
//$ o JQuery son sinónimos!!  JQuery(document).ready() === $(document).ready()
$(document).ready(()=>{
    console.log('jquery ha cargado!');
    //Selector id: #
    var rojo = $('#rojo').css('color','red').css('background','cyan');
    var amarillo = $('#amarillo').css({background:'yellow', color:'orange'});
    var verde = $('#verde').css({background:'green', color:'white'});

    //Selector de clases: .
    var cebra =$('.cebra');
    console.log(cebra.eq(2));
    cebra.css('border','5px dashed black');

    $('.cebra').click(e=>{
        alert('Me haz cliqueado!');
    })

    //Selector de etiquetas
    $('hr').mouseover(()=>alert('pasaste encima de un hr!'));

    //Selector de elementos
    $('[name="amarillo"]').mouseover(()=>console.log('Pasaste encima del amarillo!'));
    //Evento hover
    $('#caja').css('background','yellow');
    $('#caja').hover(function () {
        $(this).css('background','red');
        
        }, function () {
            $(this).css('background','blue')
        }
    );
    //Evento click y doble click
    $('#otracaja').click(function (e) { 
        //e.preventDefault();
        $(this).css('color','red');
    });
    //Los doble click no sirve callback ()=>{}
    $('#otracaja').dblclick(function(){
        $(this).css('color','blue');
    });
});

 //Evento focus y blur
 //NO sirve la función callack ()=>{} en ellas!
 var nombre = $('#nombre');
 nombre.focus(function(e){
     e.preventDefault();
     $(this).css('border','3px solid red');
 });

 nombre.blur(function(e){
    e.preventDefault();
    $(this).css('border','3px solid transparent');
    //Veamos para que el div nombre muestre el valor ingresado
    if($(this).val() !='')
    {
        $('#datos').html( $(this).val() ); //seteando valor
        $('#datos').show();
    }
    else
    {
        $('#datos').hide();
    }
 });

 //evento mousemove, si muevo el mouse puedo generar efectos, por ej, un elemento que persige al mouse
 $(document).mousemove(function(){ //Al presionar el ratón en el elemento
    $('#sigueme').css({'left':event.clientX ,'top': event.clientY});
    //event.clientX : Es la coordenada del eje x de donde se situa el mouse
    //event.clientY es la coordenada Y de donde se mueve el ratón
    //'left' es la componente css que señala el eje x
    //top el componente css que señala el eje y
 });